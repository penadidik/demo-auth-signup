package theapps.id.demoauthsignup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoauthsignupApplication {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(DemoauthsignupApplication.class, args);
	}

}
