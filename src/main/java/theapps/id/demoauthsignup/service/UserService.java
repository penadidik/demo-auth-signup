package theapps.id.demoauthsignup.service;

import theapps.id.demoauthsignup.model.User;

public interface UserService {

    void save(User user);

    User findByUsername(String username);

}
