package theapps.id.demoauthsignup.service;

public interface SecurityService {

    String findLoggedInUsername();

    void autoLogin(String username, String password);

}
