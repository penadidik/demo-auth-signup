package theapps.id.demoauthsignup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import theapps.id.demoauthsignup.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
