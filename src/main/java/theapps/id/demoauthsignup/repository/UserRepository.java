package theapps.id.demoauthsignup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import theapps.id.demoauthsignup.model.User;

public interface UserRepository extends JpaRepository<User, Long > {

    User findByUsername(String username);

}
